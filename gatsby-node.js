const path = require('path')

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  // Create index page as well as individual item pages
  const loadEquipment = new Promise((resolve, reject) => {
    graphql(`
      {
        allContentfulEquipment(limit: 1000) {
          edges {
            node {
              slug
            }
          }
        }
      }
    `).then(result => {
      if (result.errors) reject(result.errors)

      // Create index page
      createPage({
        path: `/`,
        component: path.resolve('./src/templates/index.js'),
        context: {
          slug: '/',
        },
      })

      // Create individual item pages
      result.data.allContentfulEquipment.edges.forEach(({ node }) => {
        createPage({
          path: `/item/${node.slug}/`,
          component: path.resolve('./src/templates/item.js'),
          context: {
            slug: node.slug,
          },
        })
      })

      resolve()
    })
  })

  // Create Game page and send all Games to it
  const loadGames = new Promise((resolve, reject) => {
    graphql(`
      {
        allContentfulGame {
          edges {
            node {
              slug
              displayName
            }
          }
        }
      }
    `).then(result => {
      if (result.error) reject(result.errors)

      result.data.allContentfulGame.edges.forEach(({ node }) => {
        createPage({
          path: `/game/${node.slug}`,
          component: path.resolve('./src/templates/game.js'),
          context: {
            slug: node.slug,
            displayName: node.displayName,
          },
        })
      })
      resolve()
    })
  })

  // Create pages for each Gamer
  const loadGamers = new Promise((resolve, reject) => {
    graphql(`
      {
        allContentfulGamer {
          edges {
            node {
              slug
            }
          }
        }
      }
    `).then(result => {
      if (result.error) reject(result.error)

      result.data.allContentfulGamer.edges.forEach(({ node }) => {
        createPage({
          path: `/gamer/${node.slug}`,
          component: path.resolve('./src/templates/gamer.js'),
          context: {
            slug: node.slug,
          },
        })
      })

      resolve()
    })
  })

  return Promise.all([loadEquipment, loadGames, loadGamers])
}
