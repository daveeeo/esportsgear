exports.shouldUpdateScroll = args => {
  const windowWidth = window.innerWidth

  if (windowWidth < 750) {
    return true
  } else {
    return false
  }
}

exports.onInitialClientRender = () => {
  window.___GATSBYGRAM_INITIAL_RENDER_COMPLETE = true
}
