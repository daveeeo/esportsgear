import React, { Component } from 'react'
import { Link, graphql } from 'gatsby'
import Helmet from 'react-helmet'
import Layout from '../components/layout'
import ItemPanel from '../components/itempanel'
import { fetchStreamerClips } from '../utils/twitch'
import { SocialIcon } from 'react-social-icons'
import styles from '../templates/styles/gamer.module.scss'
import { images } from '../utils/constants'

const ProfileContent = ({ items, location }) => (
  <section className={styles.content}>
    <div className={styles.container}>
      <div className={styles.body}>
        {items &&
          items.map(item => (
            <ItemPanel key={item.id} item={item} location={location} />
          ))}
      </div>
    </div>
  </section>
)

const ProfileHeader = ({ gamer, clips, loading }) => (
  <section
    className={styles.header}
    style={
      images[gamer.games[0].slug]
        ? {
            background: `url('${
              images[gamer.games[0].slug].bg
            }') no-repeat 50%`,
            backgroundSize: 'cover',
          }
        : {
            background: `url('${images['fortnite'].bg}') no-repeat 50%`,
            backgroundSize: 'cover',
          }
    }
  >
    <div className={styles.container}>
      <div className={styles.displayImage}>
        <img src={gamer.imageUrl} alt={gamer.name} />
      </div>

      <div className={styles.profile}>
        <div className={styles.details}>
          <h1 className={styles.name}>{gamer.name}</h1>
          <div className={styles.social}>
            {gamer.twitchUrl && (
              <SocialIcon
                url={gamer.twitchUrl}
                style={{ height: 28, width: 28 }}
              />
            )}

            {gamer.youTubeUrl && (
              <SocialIcon
                url={gamer.youTubeUrl}
                style={{ height: 28, width: 28 }}
              />
            )}

            {gamer.instagramUrl && (
              <SocialIcon
                url={gamer.instagramUrl}
                style={{ height: 28, width: 28 }}
              />
            )}

            {gamer.facebookUrl && (
              <SocialIcon
                url={gamer.facebookUrl}
                style={{ height: 28, width: 28 }}
              />
            )}
          </div>
        </div>

        <span className={styles.team}>{gamer.team && gamer.team[0].name}</span>

        <div className={styles.games}>
          {gamer.games.map(game => (
            <Link key={game.slug} to={`/game/${game.slug}`}>
              {game.displayName === "PLAYERUNKNOWN'S BATTLEGROUNDS"
                ? 'PUBG'
                : game.displayName}
            </Link>
          ))}
        </div>
      </div>

      <div className={styles.videos}>
        <h4>Most Popular Twitch Clips</h4>
        <div className={styles.videosContainer}>
          {loading && <div>Loading clips...</div>}
          {clips &&
            clips.length > 0 &&
            clips.map(clip => (
              <div key={clip.id} className={styles.video}>
                <a href={clip.url} target="_blank" rel="noopener noreferrer">
                  <img src={clip.thumbnail_url} alt={`Twitch clip`} />
                </a>
              </div>
            ))}
          {clips &&
            clips.length === 0 && (
              <div>This gamer doesn't have any clips on Twitch.</div>
            )}
        </div>
      </div>
    </div>
    <div className={styles.nav}>
      <h2>Equipment</h2>
    </div>
  </section>
)

export default class Gamer extends Component {
  state = {
    streamerLogin: this.props.data.contentfulGamer.twitchUrl
      ? this.props.data.contentfulGamer.twitchUrl.split('/').pop()
      : '',
    clips: null,
    loading: null,
  }

  componentDidMount() {
    this.fetchClips(this.state.streamerLogin, 4)
  }

  fetchClips = async (streamerLogin, count) => {
    this.setState({ loading: true })
    const clips = await fetchStreamerClips(streamerLogin, count)
    this.setState({ clips, loading: false })
  }

  render() {
    const { clips, loading } = this.state
    const { location } = this.props
    const gamer = this.props.data.contentfulGamer
    const items = gamer.equipment

    return (
      <Layout {...this.props}>
        <Helmet>
          <title>{gamer.name}</title>
        </Helmet>
        <div className={styles.wrapper}>
          <ProfileHeader gamer={gamer} clips={clips} loading={loading} />
          <ProfileContent items={items} location={location} />
        </div>
      </Layout>
    )
  }
}

export const query = graphql`
  query($slug: String!) {
    contentfulGamer(slug: { eq: $slug }) {
      name
      twitchUrl
      youTubeUrl
      instagramUrl
      facebookUrl
      imageUrl
      imageFilename
      slug
      team {
        name
      }
      games {
        name
        displayName
        slug
      }
      equipment {
        id
        name
        slug
        imageUrl
        imageFilename
        category {
          name
        }
        gamers {
          id
          name
          imageUrl
          imageFilename
          slug
        }
      }
    }
  }
`
