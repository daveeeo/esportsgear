import React from 'react'
import { graphql } from 'gatsby'
import Helmet from 'react-helmet'
import Layout from '../components/layout'
import Hero from '../components/hero'
import EquipmentTable from '../components/equipmenttable'

const Game = props => {
  const { data, pageContext } = props
  const items = data.equipment.edges
  const categories = data.categories.edges

  items.forEach(({ node }) => {
    node.gamers = node.gamers.filter(gamer =>
      gamer.gamesList.includes(pageContext.displayName)
    )
  })

  items.sort((a, b) => b.node.gamers.length - a.node.gamers.length)

  return (
    <Layout {...props}>
      <Helmet>
        <title>{pageContext.displayName} Pro eSports Gear</title>
      </Helmet>
      <Hero game={pageContext.displayName} items={items} {...props} />
      <div className="container-grid two-columns">
        {categories.map(({ node }) => (
          <EquipmentTable
            key={node.name}
            title={node.name}
            game={pageContext.displayName}
            items={items}
            {...props}
          />
        ))}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query($displayName: String!) {
    equipment: allContentfulEquipment(
      filter: { gamers: { elemMatch: { gamesList: { eq: $displayName } } } }
    ) {
      edges {
        node {
          id
          name
          slug
          imageUrl
          imageFilename
          brand {
            name
          }
          category {
            name
          }
          gamers {
            id
            name
            imageUrl
            imageFilename
            gamesList
            slug
          }
        }
      }
    }
    categories: allContentfulCategory(sort: { fields: order }) {
      edges {
        node {
          name
        }
      }
    }
  }
`
export default Game
