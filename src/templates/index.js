import React from 'react'
import { graphql } from 'gatsby'
import Helmet from 'react-helmet'
import Layout from '../components/layout'
import Hero from '../components/hero'
import EquipmentTable from '../components/equipmenttable.js'

const Index = props => {
  const { data } = props
  const items = data.equipment.edges
  const categories = data.categories.edges
  items.sort((a, b) => b.node.gamers.length - a.node.gamers.length)

  return (
    <Layout {...props}>
      <Helmet>
        <title>esportsgear.co - Discover Popular Esports Gear</title>
      </Helmet>
      <Hero items={items} {...props} />
      <div className="container-grid two-columns">
        {categories.map(({ node }) => (
          <EquipmentTable
            key={node.name}
            title={node.name}
            items={items}
            {...props}
          />
        ))}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    equipment: allContentfulEquipment(limit: 1000) {
      edges {
        node {
          id
          name
          slug
          imageUrl
          imageFilename
          brand {
            name
          }
          category {
            name
          }
          gamers {
            id
            name
            imageUrl
            imageFilename
            slug
          }
        }
      }
    }
    categories: allContentfulCategory(sort: { fields: order }) {
      edges {
        node {
          name
        }
      }
    }
  }
`

export default Index
