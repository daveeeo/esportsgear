import React, { Component } from 'react'
import { Link, graphql, withPrefix } from 'gatsby'
import Layout from '../components/layout'
import styles from '../templates/styles/item.module.scss'
import Tooltip from 'rc-tooltip'
import 'rc-tooltip/assets/bootstrap.css'

export default class Item extends Component {
  state = {
    showAllGamers: false,
  }

  toggleGamers = () => {
    const { showAllGamers } = this.state
    this.setState({ showAllGamers: !showAllGamers })
  }

  render() {
    const { showAllGamers } = this.state
    const item = this.props.data.contentfulEquipment
    let isModal = false
    // don't render the modal on initial render just incase the user directly accesses this page

    if (
      typeof window !== `undefined` &&
      window.___GATSBYGRAM_INITIAL_RENDER_COMPLETE
    ) {
      isModal = true
    }

    return (
      <Layout {...this.props} isModal={isModal}>
        <div
          className={`${styles.wrapper} ${
            !isModal ? styles.innerPage : styles.isModal
          }`}
        >
          <div className={styles.container} onClick={e => e.stopPropagation()}>
            <div className={styles.content}>
              <div className={styles.header}>
                <span className={styles.category}>
                  {item.category && item.category.name}
                </span>
                <h1>{item.name}</h1>
              </div>
              <div className={styles.details}>
                <div className={styles.heading}>
                  <h4>
                    {item.gamers.length > 1
                      ? `${item.gamers.length} people`
                      : `${item.gamers.length} person`}{' '}
                    using this product
                  </h4>
                  {isModal &&
                    item.gamers.length > 12 && (
                      <span
                        className={styles.toggleGamers}
                        onClick={this.toggleGamers}
                      >
                        {!showAllGamers ? 'View all' : 'View less'}
                      </span>
                    )}
                </div>

                <div className={styles.gamers}>
                  {isModal && !showAllGamers
                    ? item.gamers.slice(0, 12).map(gamer => (
                        <Tooltip
                          key={gamer.name}
                          placement="bottom"
                          trigger={['hover']}
                          overlay={<span>{gamer.name}</span>}
                          mouseEnterDelay={0.3}
                          mouseLeaveDelay={0}
                          overlayClassName="dark"
                        >
                          <Link to={`/gamer/${gamer.slug}`}>
                            <img
                              className={styles.gamerImg}
                              src={withPrefix(
                                `/images/gamers/thumbs/${gamer.imageFilename}`
                              )}
                              alt={gamer.name}
                            />
                          </Link>
                        </Tooltip>
                      ))
                    : item.gamers.map(gamer => (
                        <Tooltip
                          key={gamer.name}
                          placement="bottom"
                          trigger={['hover']}
                          overlay={<span>{gamer.name}</span>}
                          mouseEnterDelay={0.3}
                          mouseLeaveDelay={0}
                          overlayClassName="dark"
                        >
                          <Link to={`/gamer/${gamer.slug}`}>
                            <img
                              className={styles.gamerImg}
                              src={withPrefix(
                                `/images/gamers/thumbs/${gamer.imageFilename}`
                              )}
                              alt={gamer.name}
                            />
                          </Link>
                        </Tooltip>
                      ))}
                </div>
              </div>
              <div className={styles.cta}>
                <a
                  className={styles.button}
                  href={item.amazonUrl}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  View on Amazon
                </a>
              </div>
            </div>
            <div className={styles.itemImage}>
              <div className={styles.container}>
                <img
                  alt={item.name}
                  src={withPrefix(`/images/gear/${item.imageFilename}`)}
                  fluid="(min-width: 640px) 640px, 100vw"
                />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export const query = graphql`
  query($slug: String!) {
    contentfulEquipment(slug: { eq: $slug }) {
      name
      amazonUrl
      imageUrl
      imageFilename
      brand {
        name
      }
      category {
        name
      }
      gamers {
        slug
        name
        imageUrl
        imageFilename
      }
    }
  }
`
