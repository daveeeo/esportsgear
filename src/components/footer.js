import React from 'react'
import styles from '../components/styles/footer.module.scss'

export default function Footer() {
  const date = new Date()
  const year = date.getFullYear()

  return (
    <footer className={styles.footer}>
      {'©'} {year} esportsgear.co
    </footer>
  )
}
