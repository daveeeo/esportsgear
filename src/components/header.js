import React, { Component } from 'react'
import { Link, StaticQuery, graphql, navigate } from 'gatsby'
import Search from './search'
import styles from '../components/styles/header.module.scss'
import { images } from '../utils/constants'
import icHome from '../images/ic_home.jpg'

function NavList({ activeLink, onSelect, location }) {
  return (
    <StaticQuery
      query={graphql`
        query {
          allContentfulGame {
            edges {
              node {
                slug
                displayName
              }
            }
          }
        }
      `}
      render={data => (
        <div
          className={`${styles.item} ${styles.hasDropdown} ${
            styles.hoverable
          } ${styles.dropdownContainer}`}
        >
          <div
            className={`${styles.link} ${styles.dropdownLink}`}
            onClick={() => onSelect('/', 'All Games')}
          >
            {location &&
            location.pathname.includes('/game/') &&
            images[activeLink.slug] ? (
              <img
                src={images[activeLink.slug].logo}
                alt={activeLink.displayName}
              />
            ) : (
              <img src={icHome} alt={activeLink.displayName} />
            )}
            <span>{activeLink.displayName}</span>
          </div>
          <div className={`${styles.dropdown} ${styles.dropdownMenu}`}>
            <div
              className={`${styles.item} ${styles.dropdownItem}`}
              onClick={() => onSelect('/', 'All Games')}
            >
              <img src={icHome} alt="All Games" />
              <span>All Games</span>
            </div>
            {data.allContentfulGame.edges.map(({ node }) => (
              <div
                key={node.slug}
                className={`${styles.item} ${styles.dropdownItem}`}
                onClick={() => onSelect(node.slug, node.displayName)}
              >
                <img src={images[node.slug].logo} alt={node.displayName} />
                <span>{node.displayName}</span>
              </div>
            ))}
          </div>
        </div>
      )}
    />
  )
}

class GamesNav extends Component {
  state = {
    activeLink: {
      slug:
        this.props.pageContext && this.props.pageContext.slug
          ? this.props.pageContext.slug
          : '/',
      displayName:
        this.props.pageContext && this.props.pageContext.displayName
          ? this.props.pageContext.displayName
          : 'All Games',
    },
  }

  updateLink = (slug, displayName) => {
    this.setState({ activeLink: { slug, displayName } })

    slug === '/' ? navigate('/') : navigate(`/game/${slug}`)
  }

  render() {
    const { activeLink } = this.state
    const { location } = this.props

    return (
      <div className={`${styles.dropdownWrapper}`}>
        <NavList
          activeLink={activeLink}
          onSelect={this.updateLink}
          location={location}
        />
      </div>
    )
  }
}

export default class Header extends Component {
  state = {
    isTop: true,
  }

  componentDidMount() {
    document.addEventListener('scroll', this.handleScroll)

    const isTop = window.scrollY < 10

    if (isTop !== this.state.isTop) {
      this.setState({ isTop })
    }
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleScroll)
    document.removeEventListener('load', this.handleScroll)
  }

  handleScroll = () => {
    const isTop = window.scrollY < 10

    if (isTop !== this.state.isTop) {
      this.setState({ isTop })
    }
  }

  handleTouchNav = e => {
    const burger = document.querySelector('.burger')
    const nav = document.querySelector('#' + burger.dataset.target)

    burger.classList.toggle('is-active')
    nav.classList.toggle('is-active')
  }

  render() {
    const { location } = this.props
    const { isTop } = this.state

    return (
      <div className={styles.header}>
        <nav
          className={`${styles.navbar} ${styles.fixed} ${
            !isTop ? styles.hasBg : ''
          }`}
          role="navigation"
          aria-label="main navigation"
        >
          <div className={`${styles.brand} ${styles.logo}`}>
            <Link to="/" className={styles.item}>
              <h1>
                Esports
                <span>Gear</span>
              </h1>
            </Link>
          </div>

          <div id="navbar" className={styles.menu}>
            <div className={styles.start}>
              <GamesNav {...this.props} />
            </div>
            <div className={`${styles.end} ${styles.menu} ${styles.search}`}>
              <Search location={location} />
            </div>
          </div>
        </nav>
      </div>
    )
  }
}
