import React from 'react'
import Swiper from 'react-id-swiper'
import ItemPanel from './itempanel'

import 'react-id-swiper/src/styles/css/swiper.css'
import './styles/slider.scss'

const EquipmentSlider = props => {
  const { items, location } = props
  const sliderItems = items.slice(0, 9) // Top 10 Items

  const params = {
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    spaceBetween: 0,
    centeredSlides: false,
  }

  return (
    <div>
      {sliderItems.length > 0 && (
        <Swiper {...params}>
          {sliderItems.map(({ node }) => (
            <ItemPanel key={node.id} item={node} location={location} />
          ))}
        </Swiper>
      )}
    </div>
  )
}

export default EquipmentSlider
