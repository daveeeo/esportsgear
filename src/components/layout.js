import React, { Component } from 'react'
import { PageRenderer } from 'gatsby'
import Helmet from 'react-helmet'
import Header from './header'
import Footer from './footer'
import '../styles/global.scss'

let Modal
import(`./itemmodal`).then((modal) => {
  Modal = modal.default
})

let windowWidth

export default class Layout extends Component {
  render() {
    const isHomePage = this.props.location.pathname === '/'
    const isGamePage = this.props.location.pathname.includes('/game/')
    const isItemPage = this.props.location.pathname.includes('/item/')
    const prevPage =
      this.props.location.state && this.props.location.state.prevPage
        ? this.props.location.state.prevPage
        : null
    let isModal = false

    if (!windowWidth && typeof window !== `undefined`) {
      windowWidth = window.innerWidth
    }

    if (this.props.isModal && windowWidth > 768) {
      isModal = true
    }

    if (isModal && Modal) {
      return (
        <React.Fragment>
          <PageRenderer location={{ pathname: prevPage ? prevPage : `/` }} />
          <Modal
            isOpen={true}
            prevPage={prevPage}
            location={this.props.location}
          >
            {this.props.children}
          </Modal>
        </React.Fragment>
      )
    }

    return (
      <div
        className={`${isHomePage ? 'is-homepage' : ''}${
          isGamePage ? 'is-gamepage' : ''
        }${isItemPage ? 'is-itempage' : ''} ${
          !isHomePage && !isGamePage ? 'is-innerpage' : ''
        }`}
      >
        <Helmet
          defaultTitle={`esportsgear.co`}
          titleTemplate={`%s | esportsgear.co`}
        >
          <meta name="twitter:site" content="@esportsgearhq" />
          <meta name="og:type" content="website" />
          <meta name="og:site_name" content="esportsgear.co" />
          <link
            rel="canonical"
            href={`https://esportsgear.co${this.props.location.pathname}`}
          />
          <html lang="en" />
        </Helmet>
        <Header {...this.props} />
        <div>{this.props.children}</div>
        <Footer />
      </div>
    )
  }
}
