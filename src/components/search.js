import React, { Component } from 'react'
import { withPrefix, navigate } from 'gatsby'
import { InstantSearch, Index, Highlight } from 'react-instantsearch/dom'
import { connectAutoComplete } from 'react-instantsearch/connectors'
import AutoSuggest from 'react-autosuggest'
import '../components/styles/search.scss'
import { IoIosSearch as SearchIcon } from 'react-icons/io'

const Search = props => {
  const { location } = props
  return (
    <InstantSearch
      appId={process.env.GATSBY_ALGOLIA_APP_ID}
      apiKey={process.env.GATSBY_ALGOLIA_API_KEY}
      indexName={process.env.GATSBY_ALGOLIA_INDEX_NAME}
    >
      <AutoComplete location={location} />
      <Index indexName={process.env.GATSBY_ALGOLIA_INDEX_NAME} />
    </InstantSearch>
  )
}

class SearchAutoSuggest extends Component {
  constructor(props) {
    super(props)

    this.state = {
      value: this.props.currentRefinement,
    }

    this.renderSuggestion = this.renderSuggestion.bind(this)
  }

  onChange = (event, { newValue }) => {
    this.setState({ value: newValue })
  }

  onSuggestionsFetchRequested = ({ value }) => {
    this.props.refine(value)
  }

  onSuggestionsClearRequested = () => {
    this.props.refine()
  }

  getSuggestionValue(hit) {
    return hit.name
  }

  renderSuggestion(hit) {
    if (hit.contentType === 'equipment') {
      return (
        <div>
          <img
            className="item-image"
            src={withPrefix(`/images/gear/thumbs/${hit.imageFilename}`)}
            alt={hit.name}
          />
          <Highlight attribute="name" hit={hit} tagName="strong" />
        </div>
      )
    }

    if (hit.contentType === 'gamer') {
      return (
        <div>
          <img
            className="item-image"
            src={withPrefix(`/images/gamers/thumbs/${hit.imageFilename}`)}
            alt={hit.name}
          />
          <Highlight attribute="name" hit={hit} tagName="strong" />
        </div>
      )
    }
  }

  onSuggestionSelected(event, { suggestion }) {
    if (suggestion.contentType === 'equipment') {
      navigate(`/item/${suggestion.slug}`, {
        state: { prevPage: this.props.location.pathname },
      })
    }

    if (suggestion.contentType === 'gamer') {
      navigate(`/gamer/${suggestion.slug}`)
    }
  }

  renderSectionTitle(section) {
    return section.index
  }

  getSectionSuggestions(section) {
    return section.hits
  }

  render() {
    const { hits } = this.props
    const { value } = this.state

    const inputProps = {
      placeholder: 'Search for an item or gamer',
      onChange: this.onChange,
      value,
      type: 'text',
      spellCheck: 'false',
    }

    return (
      <AutoSuggest
        suggestions={hits}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        onSuggestionSelected={this.onSuggestionSelected}
        getSuggestionValue={this.getSuggestionValue}
        renderSuggestion={this.renderSuggestion}
        inputProps={inputProps}
        renderSectionTitle={this.renderSectionTitle}
        renderInputComponent={renderInputComponent}
        getSectionSuggestions={this.getSectionSuggestions}
      />
    )
  }
}

const AutoComplete = connectAutoComplete(SearchAutoSuggest)

const renderInputComponent = inputProps => (
  <div>
    <SearchIcon className="icon-search" />
    <input {...inputProps} />
  </div>
)

export default Search
