import React from 'react'
import EquipmentSlider from '../components/equipmentslider'
import { images } from '../utils/constants'
import imgPopular from '../images/ic_fire.png'

import styles from '../components/styles/hero.module.scss'

export default function Hero({ items, game, pageContext, location }) {
  return (
    <section
      className={`hero ${styles.wrapper} ${styles.hasBg}`}
      style={
        images[pageContext.slug]
          ? {
              background: `url('${images[pageContext.slug].bg}') no-repeat 50%`,
              backgroundSize: 'cover',
            }
          : {
              background: `url('${images['fortnite'].bg}') no-repeat 50%`,
              backgroundSize: 'cover',
            }
      }
    >
      <div className={`${styles.container}`}>
        {game ? (
          <div className={styles.header}>
            <h3 className={styles.title}>
              <img src={imgPopular} alt="Popular Esports Gear" /> Popular Gear
              for {game}
            </h3>
          </div>
        ) : (
          <div className={styles.header}>
            <h1 className={styles.title}>Discover Popular Esports Gear</h1>
            <p className={styles.subtitle}>
              EsportsGear curates the latest and most popular gaming gear used
              by the best professional players in the Esports industry.
            </p>
          </div>
        )}

        <EquipmentSlider items={items} location={location} />
      </div>
    </section>
  )
}
