import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { Link, withPrefix } from 'gatsby'
import styles from '../components/styles/equipmenttable.module.scss'
import { RESULTS_PER_PAGE } from '../utils/constants'
import Tooltip from 'rc-tooltip'
import 'rc-tooltip/assets/bootstrap.css'

// const propTypes = {
//   category: PropTypes.string.isRequired,
// }

function GamerThumbnail({ gamer }) {
  return (
    <Tooltip
      placement="bottom"
      trigger={['hover']}
      overlay={<span>{gamer.name}</span>}
      mouseEnterDelay={0.3}
      mouseLeaveDelay={0}
      overlayClassName="light"
    >
      <Link to={`/gamer/${gamer.slug}`}>
        <img
          className={styles.gamerImg}
          src={withPrefix(`./images/gamers/thumbs/${gamer.imageFilename}`)}
          alt={gamer.name}
        />
      </Link>
    </Tooltip>
  )
}

export default class EquipmentTable extends Component {
  state = {
    page: 1,
    data: [],
  }

  handleNextPage = () => {
    const { page } = this.state

    this.setState(() => ({ page: page + 1 }))
  }

  handlePrevPage = () => {
    const { page } = this.state

    this.setState(() => ({ page: page - 1 }))
  }

  render() {
    const { title, items, location } = this.props
    const { page } = this.state

    // Calculate start and end indexes for pagination (using slice to paginate all results since Gatsby doesn't allow variables in StaticQuery calls in components)
    const start = (page - 1) * RESULTS_PER_PAGE // Page 1: 0, Page 2: 5, Page 3: 10
    const end = page * RESULTS_PER_PAGE // Page 1: 5, Page 2: 10, Page 3: 15

    let categoryItems = items.filter(
      ({ node }) => (node.category && node.category.name) === title
    )

    const categoryItemsPaginated = categoryItems.slice(start, end)

    const totalPages = Math.ceil(categoryItems.length / RESULTS_PER_PAGE) // eg. 45 / 5 = 9 pages

    return (
      <div className={styles.contentBox}>
        <div className={styles.header}>
          <h3>
            {title} ({categoryItems.length})
          </h3>
          <div className={styles.pagination}>
            {page > 1 && (
              <button
                className={styles.prevButton}
                onClick={this.handlePrevPage}
              >
                <span className={styles.icon} />
              </button>
            )}

            {page < totalPages && (
              <button
                className={styles.nextButton}
                onClick={this.handleNextPage}
              >
                <span className={styles.icon} />
              </button>
            )}
          </div>
        </div>

        <div className={styles.tableContainer}>
          <table className={styles.table}>
            {/* <thead>
              <tr>
                <th>Rank</th>
                <th>Image</th>
                <th>Product</th>
                <th>Gamers</th>
              </tr>
            </thead> */}
            <tbody>
              {categoryItemsPaginated.map(({ node }, index) => (
                <tr key={node.id}>
                  <td className={styles.ranking}>#{index + start + 1}</td>
                  <td className={styles.image}>
                    <div
                      className={styles.itemImg}
                      style={{
                        background: `url('${withPrefix(
                          `/images/gear/thumbs/${node.imageFilename}`
                        )}') center center / 80% no-repeat white`,
                      }}
                      alt={node.name}
                    >
                      {}
                    </div>
                  </td>
                  <td className={styles.name}>
                    <Link
                      to={`/item/${node.slug}/`}
                      state={{ prevPage: location.pathname }}
                    >
                      {node.name}
                    </Link>
                  </td>
                  <td className={styles.gamers}>
                    {node.gamers.slice(0, 4).map(gamer => (
                      <GamerThumbnail key={gamer.id} gamer={gamer} />
                    ))}
                    {node.gamers.length > 4 && (
                      <span className={styles.more}>
                        +{node.gamers.length - 4} others
                      </span>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}
