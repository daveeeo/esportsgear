import React from 'react'
import { Link, withPrefix } from 'gatsby'
import styles from '../components/styles/itempanel.module.scss'
import Tooltip from 'rc-tooltip'
import 'rc-tooltip/assets/bootstrap.css'

export default function ItemPanel({ item, className, location }) {
  return (
    <div className={`${className ? className : ''} ${styles.panel}`}>
      <div>
        <div className="item-image">
          <Link
            to={`/item/${item.slug}`}
            className={styles.image}
            state={{ prevPage: location.pathname }}
            style={{
              background: `url('${withPrefix(
                `/images/gear/thumbs/${item.imageFilename}`,
              )}') center center / 80% no-repeat white`,
            }}
          >
            {}
          </Link>
        </div>
        <div className={styles.title}>
          <Link
            to={`/item/${item.slug}`}
            state={{ prevPage: location.pathname }}
          >
            <h3>{item.name}</h3>
          </Link>
        </div>
        <div>
          {item.gamers.slice(0, 4).map((gamer) => (
            <Tooltip
              key={gamer.id}
              placement="bottom"
              trigger={['hover']}
              overlay={<span>{gamer.name}</span>}
              mouseEnterDelay={0.3}
              mouseLeaveDelay={0}
            >
              <Link to={`/gamer/${gamer.slug}`}>
                <img
                  className={styles.gamerImg}
                  src={withPrefix(
                    `/images/gamers/thumbs/${gamer.imageFilename}`,
                  )}
                  alt={gamer.name}
                />
              </Link>
            </Tooltip>
          ))}
          {item.gamers.length > 4 && (
            <span className={styles.more}>+ {item.gamers.length - 4}</span>
          )}
        </div>
      </div>
    </div>
  )
}
