import React from 'react'
import Modal from 'react-modal'
import { navigate } from 'gatsby'
import { IoIosClose as CloseButton } from 'react-icons/io'
import styles from '../components/styles/itemmodal.module.scss'

Modal.setAppElement(`#___gatsby`)

const ItemModal = props => {
  const { prevPage } = props
  return (
    <Modal
      isOpen={props.isOpen}
      onRequestClose={() => navigate(prevPage ? prevPage : `/`)}
      style={{
        overlay: {
          position: `fixed`,
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          backgroundColor: `rgba(0, 0, 0, 0.47)`,
          zIndex: 40,
        },
        content: {
          position: `absolute`,
          border: `none`,
          background: `none`,
          padding: 0,
          top: 0,
          bottom: 0,
          right: 0,
          left: 0,
          overflow: `auto`,
          WebkitOverflowScrolling: `touch`,
        },
      }}
      contentLabel="Modal"
    >
      <div
        onClick={() => navigate(prevPage ? prevPage : `/`)}
        className={styles.wrapper}
        style={{}}
      >
        <div className={styles.container}>{props.children}</div>
        <CloseButton
          data-testid="modal-close"
          onClick={() => navigate(prevPage ? prevPage : `/`)}
          className={styles.closeButton}
        />
      </div>
    </Modal>
  )
}

export default ItemModal
