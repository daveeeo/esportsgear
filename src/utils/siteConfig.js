module.exports = {
  siteTitle: 'esportsgear.co | Discover Popular Esports Gear',
  siteTitleAlt: 'esportsgear.co | Discover Popular Esports Gear', // This allows an alternative site title for SEO schema.
  publisher: 'esportsgear.co', // Organization name used for SEO schema
  siteDescription:
    'EsportsGear curates the latest and most popular gaming gear used by the best professional players in the Esports industry.',
  siteUrl: 'https://esportsgear.co', // Site domain. Do not include a trailing slash! If you wish to use a path prefix you can read more about that here: https://www.gatsbyjs.org/docs/path-prefix/
  author: 'esportsgear.co', // Author for RSS author segment and SEO schema
  authorUrl: 'https://esportsgear.co/', // URL used for author and publisher schema, can be a social profile or other personal site
  userTwitter: '@EsportsGearGG', // Change for Twitter Cards
  shortTitle: 'EsportsGear', // Used for App manifest e.g. Mobile Home Screen
  siteLogo: '/logos/logo-512.png', // Logo used for SEO, RSS, and App manifest
  backgroundColor: '#283143', // Used for Offline Manifest
  themeColor: '#0090f1', // Used for Offline Manifest
  copyright: 'Copyright © 2018 esportsgear.co', // Copyright string for the RSS feed
}
