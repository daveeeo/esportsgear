const clientId = process.env.TWITCH_CLIENT_ID

function handleError(error) {
  console.warn(error)
  return null
}

export async function fetchBroadcasterId(streamerLogin) {
  const encodedURI = window.encodeURI(
    `https://api.twitch.tv/helix/users?login=${streamerLogin}`
  )
  const response = await fetch(encodedURI, {
    headers: { 'Client-ID': clientId },
  })
  const streamer = await response.json()
  const streamerObj = Object.assign({}, ...streamer.data)
  return streamerObj.id
}

export async function fetchStreamerClips(streamerLogin, count) {
  const streamerId = await fetchBroadcasterId(streamerLogin)
  const encodedURI = window.encodeURI(
    `https://api.twitch.tv/helix/clips?broadcaster_id=${streamerId}&first=${count}`
  )
  const response = await fetch(encodedURI, {
    headers: { 'Client-ID': clientId },
  }).catch(handleError)
  const clips = await response.json()

  return clips.data
}
