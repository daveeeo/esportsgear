import bgPubg from '../images/bg_pubg.jpg'
import bgOverwatch from '../images/bg_overwatch.jpg'
import bgCsgo from '../images/bg_csgo.jpg'
import bgFortnite from '../images/bg_fortnite.jpg'
import logoPubg from '../images/ic_pubg.jpg'
import logoOverwatch from '../images/ic_overwatch.jpg'
import logoCsgo from '../images/ic_csgo.jpg'
import logoFortnite from '../images/ic_fortnite.jpg'

export const RESULTS_PER_PAGE = 5

export const images = {
  overwatch: {
    bg: bgOverwatch,
    logo: logoOverwatch,
  },
  'playerunknowns-battlegrounds': {
    bg: bgPubg,
    logo: logoPubg,
  },
  fortnite: {
    bg: bgFortnite,
    logo: logoFortnite,
  },
  'counter-strike-global-offensive': {
    bg: bgCsgo,
    logo: logoCsgo,
  },
}
