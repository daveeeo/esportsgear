import Typography from 'typography'
import typographyTheme from 'typography-theme-github'

const typography = new Typography(typographyTheme)

export default typography
