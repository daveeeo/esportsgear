import React from 'react'
import Helmet from 'react-helmet'
import Layout from '../components/layout'

const NotFoundPage = (props) => (
  <Layout {...props}>
    <Helmet>
      <title>404 - Page Not Found</title>
      <meta name="description" content="Page not found" />
    </Helmet>
    <div style={{ padding: '1.5rem', marginTop: '4.5rem' }}>
      <h2>Four oh four!</h2>
      <p>Looks like this page can't be found :(</p>
    </div>
  </Layout>
)

export default NotFoundPage
